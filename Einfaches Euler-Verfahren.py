# -*- coding: utf-8 -*-
# Einfaches Euler-Verfahren zur numerischen Lösung der Differentialgleichung y'=ay

a = -0.5     # Parameter der DGL
y0 = 2.0     # Anfangswert
z = 3.0      # Intervall [0,z]
N = 10       # Teilintervalle
h = z/N      # Zeitschrittweite

y = y0       # Anfangsbedingungen
t = 0

for k in range(0,N):
    t = t+h
    y = y+h*a*y     #Euler-Verfahren
    
print '%6.1f %6.1f' % (t,y)