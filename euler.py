# Autor: Alexander Mundt, Kevin Gauda
# coding=utf-8

import numpy as np
import matplotlib.pyplot as plt

h = 0.01 #Schrittweite
T=50 #Endzeit
x0 = [2.,1.,-4.] #beliebige Anfangswerte
A=np.array([[-10.,0.,0.],[0., -15., 0.],[0., 0., -9.]]) #mxm-Array, Groesse beliebig festlegbar
m=len(A)

def function(A,x,t,i): #Diese Funktion wird im Eulerverfahren aufgerufen. Sie stellt das Gleichungssystem dar.
  y = x[i] 
  #y.transpose 
  return np.dot(A,y)


def euler(function,T,h,x0):
  t = np.zeros(T)  #Array mit T Zeilen, wobei alle Eintraege Null sind.
  x = np.zeros([T,m]) # Txm-Array voll mit Nullen. x ist eine Txm Matrix. dabei wird die i-te Zeile die Funktionswerte im i-ten Zeitschritt sein 
  x[0] = x0 #Anfangswerte fuer t und x
  t[0] = 0
  for i in range(0,T-1):
    x[i+1] = x[i] + h*function(A,x,t,i) # fuer alle Komponenten . die matrix x wird zeilenweise nach unten mit den werten der Funktion aufgefüllt
    t[i+1] = t[i] + h 
  plt.figure(1)    #ab hier wird geplottet!
  plt.plot(t,x)
  plt.show()
   
euler(function,T,h,x0) #Aufrufen der Funktion, damit ueberhaupt was passiert.
