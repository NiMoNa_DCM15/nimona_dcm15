# Autor: Alexander Mundt, Kevin Gauda
# coding=utf-8

import numpy as np
import matplotlib.pyplot as plt
import math
import Tkinter as tk


h = 0.01#Schrittweite
T=6. #Endzeit
n_t = int(T/h)
x0 = [4.,1.,2.,15.,4.] #beliebige Anfangswerte

A=np.array([[-2.,0.,0.,0.,0.],[-0.5, -3.,-4.,0.,0.],[0.,0.,-1.,0.,-0.7],[0.,2.,0.,-0.75,4.],[0.,0.,2.,-4.,-3.]]) #mxm-Array, Größe beliebig festlegbar
B=np.zeros([5,5])
B[1][2] = 3.
B[3][1] = 1.5

kappa = 0.65
gamma = 0.41
tau = 0.98
alpha= 0.32
rho = 0.34

#s,f,v,q
# noch für alle nodes gleich
Hemo_const = np.array([[-kappa,-gamma,0.,0.],[1.,0.,0.,0.],[0.,1./tau,0.,0.],[0.,0.,0.,0.]])
Hemo_initial = np.array([0.,1.,1.,1])



m=len(A)


# --------	Funktionen	---------------
#def f(A,y,i)
def f_neuro(A,y,t,*empty): #Diese Funktion wird im Eulerverfahren aufgerufen. Sie stellt das Gleichungssystem dar.
  def u2(t):
      return math.sin(t) *math.exp(math.sin(t))
  
  
  #y = x[i] 
  #y.transpose 
  return np.dot(A,y)+np.dot(B,y)*u2(t)

def hemodynamicsystem(A,y,t,z):
  s = y[0]
  f = y[1]
  v = y[2]
  q =  y[3]
   
  t_int = int(t/h) 

  hemo_nonlinear = np.array([z[t_int]+gamma,0.,-1.*np.power(v,(1./alpha))/tau, f/(tau*rho)*(1.-np.power( (1.-rho),(1./f)) ) - np.power(v,(1./alpha-1.))*q/tau])
  return np.dot(A,y) + hemo_nonlinear


def solve(f,method,T,h,x0,A,*additional):
  t = np.zeros(T)  #Array mit T Zeilen, wobei alle Einträge Null sind.
  m = len(A)
  x = np.zeros([T,m]) # Txm-Array voll mit Nullen. x ist eine Txm Matrix. dabei wird die i-te Zeile die Funktionswerte im i-ten Zeitschritt sein 
  x[0] = x0 #Anfangswerte fuer t und x
  t[0] = 0.
  if not additional:
      additional= [0.]
   
  return method(f,h,x,t,A,additional[0])

def euler(f,h,x,t,A,*additional):
  for i in range(0,len(x)-1):
    x[i+1] = x[i] + h*f(A,x[i],t[i]) # fuer alle Komponenten . die matrix x wird zeilenweise nach unten mit den werten der Funktion aufgefüllt
    t[i+1] = t[i] + h 
  return [t,x]

def euler_improved(f,h,x,t,A,*additional):
  for i in range(0,len(x)-1):
    y = x[i] + h*f(A,x[i],t[i])
    t[i+1] = t[i] + h 
    x[i+1] = x[i] + 0.5*h*(f(A,x[i],t[i])+f(A,y,t[i]))     
  return [t,x]

def RK4(f,h,x,t,A,*additional):
  for i in range(0,len(x)-1):
    k1 = f(A,x[i],t[i],additional[0])
    k2 = f(A, x[i]+h*0.5*k1 , t[i]+0.5*h,additional[0]) 
    k3 = f(A, x[i]+h*0.5*k2 , t[i]+0.5*h,additional[0])
    k4 = f(A, x[i]+h*k3, t[i]+h,additional[0])
    t[i+1] = t[i] + h 
    x[i+1] = x[i] + h/6.*(k1+2.*k2+2.*k3+k4)
 
  return [t,x]


def plot(t,x,color,label_name):
  plt.figure(1)    #ab hier wird geplottet!
  obj, = plt.plot(t,x,color, label=label_name)
  return obj
   
# -------	Aufrufen der Funktion, damit ueberhaupt was passiert	---------


ausgabe = solve(f_neuro,euler,n_t,h,x0,A) 
t = ausgabe[0]
x = ausgabe[1]
#plt.plot(t,x, 'b', label='Euler')



ausgabe = solve(f_neuro,euler_improved,n_t,h,x0,A) 
t = ausgabe[0]
x = ausgabe[1]
#plt.plot(t,x,'r',label='Improved Euler')


ausgabe = solve(f_neuro,RK4,n_t,h,x0,A) 
t = ausgabe[0]
x = ausgabe[1]
#plt.plot(t,x,'g',label='RK4')
#plt.legend()
xplot = np.ndarray.transpose(x)



ax1 = plt.subplot2grid((5,2), (0,0), rowspan = 5)
axr = [ plt.subplot2grid((5,2), (0,1)),
	plt.subplot2grid((5,2), (1,1)),
	plt.subplot2grid((5,2), (2,1)),
	plt.subplot2grid((5,2), (3,1)),#]#s,
	plt.subplot2grid((5,2), (4,1))]

for i in xrange(0,len(xplot)):
   ax1.plot(t,xplot[i], label = 'Node '+str(i))
   ax1.set_title('Neuronal Activity')
   ax1.legend()     
 
#Berechnung des Hemodyniamscihsetsndf System für die Nodes

ausgabe = []
uebergabe = np.ndarray.transpose(x)

# Berechnung der State-Variablen des Hemodynamischen Systems für jeden einzelnen Node
for i in xrange(0,len(uebergabe)):
    # In der hier gewählten Fassung zur Berechnung ist die Übergabe des vorher berechneten z-Wertes des Nodes wichtig
    # Die Transponierung ist erforderlich, um die z-Werte der verschiedenen timesteps in einer Liste zu speichern;
    # vorher sind die z-Werte in einer Matrix gespeichert, in der die Werte zu jedem Node in einer Spalte stehen, 
    # also über mehrere Listen verteilt stehen
    # Eigentlich aber nicht in jedem Schritt der Schleife notwendig (also nur einmal)...

    
    z = uebergabe[i]
    # i-te Liste in z ist die Aktivität im i-ten Node

    # Jetzt Lösung des Systems mit RK4 und Anfangswerten
    ausgabe.append(solve(hemodynamicsystem,RK4,n_t,h,Hemo_initial,Hemo_const,z))   		
    agt = np.ndarray.transpose(ausgabe[i][1]) # Hilfsvariable, die die Werte-List zwischenspeichert zur Übersichtlichkeit
    # Jede Line wird einzeln gekloppelt, eventuell geschickter möglich, wichtig für korrekte Legende
    axr[i].plot(t,agt[0],'b', label = 's')
    axr[i].plot(t,agt[1],'g', label = 'f')
    axr[i].plot(t,agt[2],'y', label = 'q')
    axr[i].plot(t,agt[3],'black', label = 'v')	
    axr[i].set_title('Hemodynamische State-Variable in Node ' + str(i))


axr[1].legend(bbox_to_anchor=(0.98, 0.98),
           bbox_transform=plt.gcf().transFigure)
    

#plt.axis([0,4,-14.*np.mean(x),14.*np.mean(x)])
#plt.axis([0,4,-4,4])


plt.show()
plt.close()


# ----------	Visualisierung der Aktivität in den Nodes	---------------

window = tk.Tk()
cw = 800
ch = 600
fullcirclesize = 75
canvas = tk.Canvas(window, width=cw, height=ch, background="white")
window.title('Dynamik der Nodes')
cycle_period = (100)

xposition =  (cw-4*fullcirclesize)/3+fullcirclesize
yposition =  (ch-4*fullcirclesize)/3+fullcirclesize
def circle(canvas,x,y, r, color):
   id = canvas.create_oval(x-r,y-r,x+r,y+r, fill = color)
   return id

absolutevalue = np.abs(xplot)
maxsize= max([max(i) for i in absolutevalue])
canvas.grid(row=0, column=0)
for i in xrange(0,len(xplot[0])):
    for j in xrange(0,len(xplot)):
        circle(canvas,xposition*((j/2)%2+1),yposition*(j%2+1),int(absolutevalue[j][i]/maxsize*fullcirclesize),'black')
# hier noch mal eventuell variabler machen. Multiplikation mit xpos und ypos geht von 2x2 Grid für die Werte aus 
    canvas.after(cycle_period)
    canvas.update()
    canvas.delete(tk.ALL)

   
#canvas.pack()

window.mainloop()

