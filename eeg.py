# coding=utf-8

import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import special
import matplotlib.pyplot as plt

h = 0.001#Schrittweite
T=0.5 #Endzeit
n_t = int(T/h)
x0 = [0.,0.,0.]

e0 = 0.5
r = 0.56

H_e = 4.
Tau_e = 0.008
H_i = 32.
Tau_i = 0.016
gamma1 = 128.
gamma2 = 4./5.*gamma1
gamma3 = 1./4.*gamma1
gamma4 = 1./4.*gamma1
dt_ii = 0.002
dt_ij = 0.016

ny1 = 1.
ny2 = 16.


C_l = np.array([[0.,4.,0.],[4, 0.,0.],[0.,0.0,0]])
C_f = np.array([[0.,32,0.],[0., 0.,32.],[0.,0,0]])
C_b = np.array([[0.,16.,0.],[0.0, 0.,16.],[16.,0.0,0]])

C_u = np.array([1.1,1.2,1.3])

def solve(f,method,T,h,x0):
   t = np.zeros(T)  #Array mit T Zeilen, wobei alle Einträge Null sind.
   m = len(x0)
   x = np.zeros([len(t),9,m]) # Txm-Array voll mit Nullen. x ist eine Txm Matrix. dabei wird die i-te Zeile die Funktionswerte im i-ten Zeitschritt sein 
   x[0][0] = x0 #Anfangswerte fuer t und x
   t[0] = 0.
    
   return method(f,h,x,t,)
 
 
def RK4(f,h,x,t):
   for i in range(0,len(x)-1):
     k1 = f(x[i],t[i],x)
     k2 = f(x[i]+h*0.5*k1 , t[i]+0.5*h,x) 
     k3 = f(x[i]+h*0.5*k2 , t[i]+0.5*h,x)
     k4 = f(x[i]+h*k3, t[i]+h,x)
     t[i+1] = t[i] + h 
     x[i+1] = x[i] + h/6.*(k1+2.*k2+2.*k3+k4)
     #print t
  
   return [t,x]
 
def f_eeg(x_i, t,x_t):
	# Rechte Seite der ODEs
	if ( t-dt_ii > 0.) :
		t_int_ii = int((t-dt_ii)/h)
	else:
		t_int_ii = int(t/h)
	if ( t-dt_ij > 0.):
		t_int_ij = int( (t-dt_ij)/h )
	else:
		t_int_ij = int(t/h)
	t_int_ij = int((t-dt_ij)/h)
	#x_i = np.zeros([9,len(x[0][0])])
	#x_i = x[t_int]

	x_i_PUNKT = np.zeros([9,len(x_i[0])])

	x_i_PUNKT[0] = x_i[5]-x_i[6]
	x_i_PUNKT[1] = x_i[4]
	x_i_PUNKT[2] = x_i[5]
	x_i_PUNKT[3] = x_i[6]
#	x_i_PUNKT[4] = H_e/Tau_e*( np.dot(C_u,u(t)) + np.dot(C_f+C_l+np.identity(len(x_i[0]))*gamma1,S(x_i[0])))- 2./Tau_e*x_i[4]-1./np.power(Tau_e,2.)*x_i[1]
	x_i_PUNKT[4] = H_e/Tau_e*( np.dot(C_u,u(t)) + np.dot(C_f+C_l,S(x_t[t_int_ij][0]))+gamma1*S(x_t[t_int_ii][0]))- 2./Tau_e*x_i[4]-1./np.power(Tau_e,2.)*x_i[1]

#	x_i_PUNKT[5] = H_e/Tau_e*((np.dot(C_b +C_l,S(x_i[0])) + gamma2*S(x_i[1]))) - 2./Tau_e*x_i[5] - 1./np.power(Tau_e,2.)*x_i[2]
	x_i_PUNKT[5] = H_e/Tau_e*((np.dot(C_b +C_l,S(x_t[t_int_ij][0])) + gamma2*S(x_t[t_int_ii][1]))) - 2./Tau_e*x_i[5] - 1./np.power(Tau_e,2.)*x_i[2]


	#x_i_PUNKT[6] = H_i/Tau_i*gamma4*S(x_i[7]) - 2./Tau_i*x_i[6] - 1./np.power(Tau_i,2.) * x_i[3]
	x_i_PUNKT[6] = H_i/Tau_i*gamma4*S(x_t[t_int_ii][7]) - 2./Tau_i*x_i[6] - 1./np.power(Tau_i,2.) * x_i[3]
	x_i_PUNKT[7] = x_i[8]
#	x_i_PUNKT[8] = H_e/Tau_e*(np.dot(C_b+C_l+gamma3*np.identity(len(x_i[0])),S(x_i[0]))) - 2./Tau_e*x_i[8] - 1./np.power(Tau_e,2.)*x_i[7]
	x_i_PUNKT[8] = H_e/Tau_e*(np.dot(C_b+C_l ,S(x_t[t_int_ij][0])) + gamma3*S(x_t[t_int_ii][0])) - 2./Tau_e*x_i[8] - 1./np.power(Tau_e,2.)*x_i[7]
		
# darauf achten dass die Funktion S(x) ndarray komp. ist
	return x_i_PUNKT

def S(x):
	result = e0*np.tanh(r*x/2)
	return result

def u(t):
	term1 = np.power(ny2,ny1)/special.gamma(ny1)*np.power(t,ny1-1.)*np.exp(-ny2*t) 
	term2 = 0. # eigentlich cos Summe
	return term1

[t,ausgabe] = solve(f_eeg,RK4,n_t,h,x0)
#print t
#print ausgabe
nur_x0 = np.zeros([len(t),len(ausgabe[0][0])])
for i in xrange(0,len(ausgabe)):
	nur_x0[i] = ausgabe[i][0]
plt.plot(t,nur_x0)
# nochmal u(t) plotten
plt.show()
	
